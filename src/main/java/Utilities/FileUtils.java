package Utilities;

import org.apache.catalina.mbeans.ContextEnvironmentMBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource("properties/fileSetting.properties")
public class FileUtils {
    public static final String FILE_EXTENSION = "extension";
// TODO: cannot get properties by file name => please check later
//    @Autowired
//    private static Environment env;
//
//    public static String getProperty(String key) {
//        return env.getProperty(key);
//    }
}
