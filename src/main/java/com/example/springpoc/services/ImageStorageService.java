package com.example.springpoc.services;

import com.example.springpoc.constants.CommonConstant;
import org.apache.commons.io.FilenameUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StreamUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.UUID;
import java.util.stream.Stream;

@Service
public class ImageStorageService implements IStorageService {
    private final Path storageFolder = Paths.get("uploads");

    // Constructor
    public ImageStorageService() {
        try {
            Files.createDirectories(storageFolder);
        } catch (IOException e) {
            throw new RuntimeException("Cannot initialize storage", e);
        }
    }

    @Override
    public String storeFile(MultipartFile file) {
        try {
            if (file.isEmpty()) {
                throw new RuntimeException("Failed to store file empty");
            }
            if (!isImageFile(file)) {
                throw new RuntimeException("File extension is not valid");
            }
            // Check file size must be <= 5 Mb
            if (file.getSize() / 1_000_000.0f > 5.0f) {
                throw new RuntimeException("File size must be <= 5 Mb");
            }
            // File must be rename
            String _fileExetension = FilenameUtils.getExtension(file.getOriginalFilename());
            String _generatedFileName = UUID.randomUUID().toString().replace(CommonConstant.HYPHEN, CommonConstant.EMPTY);
            _generatedFileName = _generatedFileName + CommonConstant.DOT + _fileExetension;
            Path _destinationFilePath = this.storageFolder.resolve(Paths.get(_generatedFileName)).normalize().toAbsolutePath();
            if (!_destinationFilePath.getParent().equals(this.storageFolder.toAbsolutePath())) {
                throw new RuntimeException("Cannot store file outside current directory.");
            }
            try (InputStream _inputStream = file.getInputStream()) {
                Files.copy(_inputStream, _destinationFilePath, StandardCopyOption.REPLACE_EXISTING);
            }
            return _generatedFileName;
        } catch (IOException e) {
            throw new RuntimeException("Cannot initialize storage", e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.storageFolder, 1).filter(path ->{
                        return !path.equals(this.storageFolder) && !path.toString().contains("._");
                    })
                    .map(this.storageFolder::relativize);
        } catch (Exception e) {
            throw new RuntimeException("Failed to load all images url", e);
        }
    }

    @Override
    public byte[] readFileContent(String fileName) {
        try {
            Path _file = storageFolder.resolve(fileName);
            Resource _resource = new UrlResource(_file.toUri());
            if (_resource.exists() || _resource.isReadable()) {
                byte[] _bytes = StreamUtils.copyToByteArray(_resource.getInputStream());
                return _bytes;
            } else {
                throw new RuntimeException("Could not read file:" + fileName);
            }
        } catch (IOException e) {
            throw new RuntimeException("Could not read file:" + fileName, e);
        }
    }

    @Override
    public void deleteAllFiles() {

    }

    private boolean isImageFile(MultipartFile file) {
        // Let install FileNameUtils
        String _fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        return Arrays.asList(new String("png,jpg,jpeg,bmp").split(CommonConstant.COMMA)).contains(_fileExtension);
    }
}
