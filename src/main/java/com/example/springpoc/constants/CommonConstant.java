package com.example.springpoc.constants;

public class CommonConstant {

    // Default constructor prevent create new instant
    private CommonConstant() {
    }

    public static final String EMPTY = "";
    public static final String DOT = ".";
    public static final String COMMA = ",";
    public static final String HYPHEN = "-";
}
