package com.example.springpoc.controllers;

import com.example.springpoc.services.ImageStorageService;
import com.example.springpoc.models.ResponseObject;
import org.apache.tomcat.util.http.fileupload.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/api/v1/fileupload")
public class FileUploadController {
    @Autowired
    private ImageStorageService imageStorageService;

    // this controller receive file/image from client
    @PostMapping("")
    public ResponseEntity<ResponseObject> uploadFile(@RequestParam("file") MultipartFile file) {
        try {
            String _generateFileName = imageStorageService.storeFile(file);
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("failed", "upload file successfully", _generateFileName)
                    );
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_IMPLEMENTED).body(
                    new ResponseObject("failed", "upload file failed", "")
                    );
        }
    }

    @GetMapping("/files/{fileName:.+}")
    public ResponseEntity<byte[]> readDetailFile(@PathVariable String fileName) {
        try {
            byte [] _bytes = imageStorageService.readFileContent(fileName);
            return ResponseEntity.ok().contentType(MediaType.IMAGE_JPEG).body(_bytes);
        } catch (Exception e) {
            return ResponseEntity.noContent().build();
        }
    }

    @GetMapping("")
    public ResponseEntity<ResponseObject> getUploadFile() {
        try {
            List <String> _urls = imageStorageService.loadAll().map(path -> {
                String _urlPath = MvcUriComponentsBuilder.fromMethodName(FileUploadController.class,"readDetailFile", path.getFileName().toString()).build().toUri().toString();
                return _urlPath;
            }).collect(Collectors.toList());
            return ResponseEntity.ok(new ResponseObject("ok", "List file successfully", _urls));
        } catch (Exception e) {
            return ResponseEntity.ok(new ResponseObject("failed", "List file successfully", ""));
        }
    }
}
