package com.example.springpoc.controllers;

import com.example.springpoc.models.Product;
import com.example.springpoc.models.ResponseObject;
import com.example.springpoc.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping(path = "/api/v1/products")
public class ProductController {
    // DI: Dependency Injection
    @Autowired
    private ProductRepository productRepository;

    @GetMapping("")
    List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    @GetMapping("/{id}")
    ResponseEntity<ResponseObject> getProductById(@PathVariable Long id) {
        Optional<Product> _product = productRepository.findById(id);
        return _product.isPresent() ?
                ResponseEntity.status(HttpStatus.OK).body(
                        new ResponseObject("ok", "Query product successfully", _product)
                        // You can replace ok with your business error code
                ) :
                ResponseEntity.status(HttpStatus.NOT_FOUND).body(
                        new ResponseObject("failed", "Query product not found", "")
                );
    }

    @PostMapping("/new")
    ResponseEntity<ResponseObject> insertProduct(@RequestBody Product product) {
        List<Product> _listProductByName = productRepository.findByName(product.getName());
        if (_listProductByName.isEmpty()) {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("ok", "insert successfully", productRepository.save(product))
            );
        } else {
            return ResponseEntity.status(HttpStatus.OK).body(
                    new ResponseObject("failed", "duplicate product name", "")
            );
        }
    }

    // update and insert = upseart => update if product was found, otherwise insert new product
    @PutMapping("/{id}")
    ResponseEntity<ResponseObject> upseartProduct(@RequestBody Product product, @PathVariable Long id) {
        Optional<Product> _product = Optional.of(productRepository.findById(id).map(product1 -> {
            product1.setName(product.getName());
            product1.setPrice(product.getPrice());
            product1.setYear(product.getYear());
            product1.setUrl(product.getUrl());
            return productRepository.save(product1);
        }).orElseGet(() -> {
            return productRepository.save(product);
        }));

        return ResponseEntity.status(HttpStatus.OK).body(
                new ResponseObject("ok", "Update product successfully", _product)
        );
    }

    @DeleteMapping("/{id}")
    ResponseEntity<ResponseObject> deteleProduct(@PathVariable Long id) {
        Boolean _existingProduct = productRepository.existsById(id);
        if (_existingProduct) {
            productRepository.deleteById(id);
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseObject("ok", "Delete successfully", ""));
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new ResponseObject("failed", "Cannot find the product", ""));
        }
    }
}
