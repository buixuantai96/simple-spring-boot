package com.example.springpoc.models;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="PRODUCT_TBL")
public class Product implements Serializable {
    private static final long serialVersionUID = -7524171554828064147L;

    @Id
    @SequenceGenerator(name = "product_sequence", sequenceName = "product_sequence", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "product_sequence")
    private Long id;

    // validate = constraint
    @Column(nullable = false, unique = true, length = 300)
    private String name;
    private int year;
    private Double price;
    private String url;

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", year=" + year +
                ", price=" + price +
                ", url='" + url + '\'' +
                '}';
    }

    // Default constructor
    public Product(String name, int year, Double price, String url) {
        this.name = name;
        this.year = year;
        this.price = price;
        this.url = url;
    }

    public Product() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
