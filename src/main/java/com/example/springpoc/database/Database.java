package com.example.springpoc.database;

import com.example.springpoc.models.Product;
import com.example.springpoc.repositories.ProductRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Database {
    private static final Logger logger = LoggerFactory.getLogger(Database.class);

    @Bean
    CommandLineRunner initDatabase(ProductRepository productRepository) {
        return new CommandLineRunner() {
            @Override
            public void run(String... args) throws Exception {
//                Product productA = new Product("ipad", 1996, 300D, "testipad.com");
//                Product productB = new Product("iphone", 2020, 300D, "testipad.com");
//                System.out.println("Insert data: " + productRepository.save(productA));
//                System.out.println("Insert data: " + productRepository.save(productB));
            }
        };
    }
}
