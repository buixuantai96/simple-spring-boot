package com.example.springpoc;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@TestPropertySource("/properties/fileSetting.properties")
public class FilePropertyInjectionUnitTest {
    @Value("${extension}")
    private String extension;

    @Test
    public void whenFilePropertyProvided_thenProperlyInjected() {
        assertThat(extension).isEqualTo("png,jpg,jpeg,bmp");
    }
}
